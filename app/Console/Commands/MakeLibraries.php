<?php

/**
 * @author mtaufiikh@gmail.com
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MakeLibraries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:lib {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make Libraries';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = $this->argument('name');

        $buffer = file_get_contents(base_path('examples/Library.php'));
        $buffer = str_replace('Example', $name, $buffer);
        file_put_contents(base_path("app/Libraries/{$name}.php"), $buffer);

        $this->info('Successfully create Library.');
    }
}

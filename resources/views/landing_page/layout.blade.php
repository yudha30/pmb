<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- CSS dependencies -->
	<link rel="stylesheet" type="text/css" href="{{ asset('landing_page/css/bootstrap.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('landing_page/css/font-awesome.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('landing_page/css/main.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('landing_page/css/font-style.css') }}" />
	<title>PMB POLBANGTAN</title>
</head>
<body>
	<div class="pix_section pix_nav_menu pix_scroll_header normal pix-padding-v-10" data-scroll-bg="#fff">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-xs-12 pix-inner-col col-sm-10">
					<div class="pix-content">
						<nav class="navbar navbar-default pix-no-margin-bottom pix-navbar-default">
							<div class="container-fluid">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#pix-navbar-collapse" aria-expanded="false">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
									<a class="navbar-brand logo-img logo-img-a pix-adjust-height" href="#"><img src="images/main/logo-md.png" alt="" class="img-responsive pix-logo-img"></a>
								</div>
								<div class="collapse navbar-collapse" id="pix-navbar-collapse">
									<ul class="nav navbar-nav navbar-right media-middle pix-header-nav pix-adjust-height" id="pix-header-nav">
										<li class="dropdown"><a href="#step" class="pix-gray">Alur Pendaftaran</a>
										<li class="dropdown"><a href="#schedule" class="pix-gray">Jadwal</a>
										</li>
										<li class="dropdown">
											<a href="#" class="dropdown-toggle pix-gray" data-toggle="dropdown">
												Berkas
											</a>
											<ul class="dropdown-menu dropdown-menu-left">
												<li><a href="#requirement" class="null" data-toggle="null">Berkas dan Persyaratan</a>
												</li>
												<li><a href="#download" class="null" data-toggle="null">Unduh Juklak</a>
												</li>
												<li><a href="#footer" class="null" data-toggle="null">Berkas Lain</a>
												</li>
											</ul>
										</li>
										<li class="dropdown"><a href="#footer" class="pix-gray">Kontak</a></li>
									</ul>
								</div>
							</div>
						</nav>
					</div>
				</div>
				<div class="col-md-2 col-xs-12 pix-inner-col col-sm-2">
					<div class="pix-content pix-adjust-height text-right" id="pix-header-btn">
						<div class="pix-header-item2 pix-padding-v-10">
							<a href="#" class="btn btn-sm pix-line pix-yellow-line small-text">
								<span class="pix_edit_text">
									<strong>Daftar Sekarang</strong>
								</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    @yield('content')

	<div class="pix_section pix-padding-v-50" style="padding-top: 100px !important" id="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-xs-12">
                    <h4 class="pix-black-gray-dark"><span class="pix_edit_text">Kontak</span></h4>
					<div style="display:flex; margin-top:5px">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm-2.29-2.333A17.9 17.9 0 0 1 8.027 13H4.062a8.008 8.008 0 0 0 5.648 6.667zM10.03 13c.151 2.439.848 4.73 1.97 6.752A15.905 15.905 0 0 0 13.97 13h-3.94zm9.908 0h-3.965a17.9 17.9 0 0 1-1.683 6.667A8.008 8.008 0 0 0 19.938 13zM4.062 11h3.965A17.9 17.9 0 0 1 9.71 4.333 8.008 8.008 0 0 0 4.062 11zm5.969 0h3.938A15.905 15.905 0 0 0 12 4.248 15.905 15.905 0 0 0 10.03 11zm4.259-6.667A17.9 17.9 0 0 1 15.973 11h3.965a8.008 8.008 0 0 0-5.648-6.667z" fill="rgba(136,136,136,1)"/></svg>
                        <span style="margin-left: 5px" class="pix-black-gray-light">www.polbangtanyoma.ac.id</span>
                    </div>
                    <div style="display:flex; margin-top:5px">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M2.243 6.854L11.49 1.31a1 1 0 0 1 1.029 0l9.238 5.545a.5.5 0 0 1 .243.429V20a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V7.283a.5.5 0 0 1 .243-.429zM4 8.133V19h16V8.132l-7.996-4.8L4 8.132zm8.06 5.565l5.296-4.463 1.288 1.53-6.57 5.537-6.71-5.53 1.272-1.544 5.424 4.47z" fill="rgba(136,136,136,1)"/></svg>
                        <span style="margin-left: 5px" class="pix-black-gray-light">info@polbangtanyoma.ac.id</span>
                    </div>
                    <div style="display:flex; margin-top:5px">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M9.366 10.682a10.556 10.556 0 0 0 3.952 3.952l.884-1.238a1 1 0 0 1 1.294-.296 11.422 11.422 0 0 0 4.583 1.364 1 1 0 0 1 .921.997v4.462a1 1 0 0 1-.898.995c-.53.055-1.064.082-1.602.082C9.94 21 3 14.06 3 5.5c0-.538.027-1.072.082-1.602A1 1 0 0 1 4.077 3h4.462a1 1 0 0 1 .997.921A11.422 11.422 0 0 0 10.9 8.504a1 1 0 0 1-.296 1.294l-1.238.884zm-2.522-.657l1.9-1.357A13.41 13.41 0 0 1 7.647 5H5.01c-.006.166-.009.333-.009.5C5 12.956 11.044 19 18.5 19c.167 0 .334-.003.5-.01v-2.637a13.41 13.41 0 0 1-3.668-1.097l-1.357 1.9a12.442 12.442 0 0 1-1.588-.75l-.058-.033a12.556 12.556 0 0 1-4.702-4.702l-.033-.058a12.442 12.442 0 0 1-.75-1.588z" fill="rgba(136,136,136,1)"/></svg>
                        <span style="margin-left: 5px" class="pix-black-gray-light">0293 - 364188</span>
                    </div>
                    <div style="display:flex; margin-top:5px">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm1-8h4v2h-6V7h2v5z" fill="rgba(136,136,136,1)"/></svg>
                        <span style="margin-left: 5px" class="pix-black-gray-light">(Jam layanan Senin - Jumat jam 08.00 - 15.30 WIB)</span>
                    </div>
				</div>
				<div class="col-md-4 col-xs-12">
					<h4 class="pix-black-gray-dark"><span class="pix_edit_text">Berkas Lain</span></h4>

                    <a href="https://box.gmedia.id/s/Zkpbezk68FkkzqK/download?path=%2F&files=DAFTAR%20MATA%20KULIAH%20NON%20RPL%20DAN%20RPL%20YOMA%20PERTANIAN%20JOGJA_1%20Agsutus.xlsx" class="pix-black-gray-light">Daftar Matakuliah Non RPL dan RPL</a>

				</div>
				<div class="col-md-4 col-xs-12">
					<h4 class="pix-black-gray-dark"><span class="pix_edit_text">Sosial Media</span></h4>
					<div>
						<a href="https://www.facebook.com/PolbangtanYoMa" class="small-social">
							<i class="pixicon-facebook3 big-icon-50 pix-slight-white"></i>
						</a>
						<a href="https://twitter.com/Polbangtan_YoMa" class="small-social">
							<i class="pixicon-twitter4 big-icon-50 pix-slight-white"></i>
						</a>
						<a href="https://www.instagram.com/polbangtanyoma/" class="small-social">
							<i class="pixicon-instagram4 big-icon-50 pix-slight-white"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Javascript -->
	<script src="{{ asset('landing_page/js/jquery-1.11.2.js') }}"></script>
	<script src="{{ asset('landing_page/js/jquery-ui.js')}}"></script>
	<script src="{{ asset('landing_page/js/bootstrap.js') }}"></script>
	<script src="{{ asset('landing_page/js/velocity.min.js') }}"></script>
	<script src="{{ asset('landing_page/js/velocity.ui.min.js') }}"></script>
	<script src="{{ asset('landing_page/js/scroll.js') }}" type="text/javascript"></script>
	<script src="{{ asset('landing_page/js/custom.js') }}"></script>
    @stack('script')
</body>
</html>

@extends('layouts.app')

@push('style')
    <style>
        td {
            padding: 3px !important;
        }
    </style>
@endpush

@section('content')

<h5>Form  03.  Daftar Riwayat Hidup Calon Mahasiswa RPL</h5>
<div class="card">
    <div class="card-body">
        <div class="text-center">
            <h4>Formulir Daftar Riwayat Hidup</h4>
            <h4> (CURRICULUM VITAE) </h4>
        </div>
        <div class="biodata mt-4">
            <h4>IDENTITAS DIRI</h4>
            <div class="p-2 table-responsive">
                <table class="table table-borderless mb-0">
                    <tr>
                        <td width="20%" valign="top">Nama</td>
                        <td width="1%" valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Tempat dan Tanggal Lahir</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Jenis Kelamin</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Status Perkawinan</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Agama</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Pekerjaan</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Alamat</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Telp./Faks.</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Alamat Rumah</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Telp./HP</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="history-education mt-4">
            <h4>RIWAYAT PENDIDIKAN</h4>
            <div class="p-2 table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Sekolah</th>
                            <th>Tahun Lulus</th>
                            <th>Jurusan / Program Studi</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="professional-training mt-4">
            <h4>PELATIHAN PROFESIONAL</h4>
            <div class="p-2 table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Tahun</th>
                            <th>Jenis Pelatihan <br> (Dalam / Luar Negeri)</th>
                            <th>Penyelenggara</th>
                            <th>Jangka Waktu</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="seminar mt-4">
            <h4>KONFERENSI/SEMINAR/LOKAKARYA/SIMPOSIUM</h4>
            <div class="p-2 table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Tahun</th>
                            <th>Judul Kegiatan</th>
                            <th>Penyelenggara</th>
                            <th>Panitia/Peserta/Pembicara</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="award mt-4">
            <h4>PENGHARGAAN/PIAGAM</h4>
            <div class="p-2 table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Tahun</th>
                            <th>Bentuk Penghargaan</th>
                            <th>Pemberi</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="organization mt-4">
            <h4>ORGANISASI PROFESI/ILMIAH</h4>
            <div class="p-2 table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Tahun</th>
                            <th>Jenis / Nama Organisasi</th>
                            <th>Jabatan / Jenjang <br> Keanggotaan</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="work-experience mt-4">
            <h4>DAFTAR RIWAYAT PEKERJAAN/PENGALAMAN KERJA</h4>
            <div class="p-2 table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama dan Alamat <br> Institusi / Perusahaan</th>
                            <th>Periode Bekerja <br> (Tgl/bln/th)</th>
                            <th>Posisi <br> Jabatan <sup>*3</sup></th>
                            <th>Uraian Tugas utama pada posisi <br> pekerjaan tersebut</th>
                            <th>Bukti yang  <br> disampaikan</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="d-flex justify-content-between">
            <div>
                <a href="javascript:void(0)" class="btn btn-lighten-secondary waves-effect width-md btn-back">Kembali</a>
            </div>
            <div>
                <a href="javascript:void(0)" class="btn btn-success waves-effect width-md waves-light mr-2 btn-verification"> Verifikasi </a>
                <a href="javascript:void(0)" class="btn btn-info waves-effect width-md waves-light btn-next"> Lanjut </a>
            </div>
        </div>
    </div>
</div>

@include('admin.verification.form3.modal')
@endsection

@push('script')
@include('admin.verification.form3.script')
@endpush

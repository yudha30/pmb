@extends('layouts.app')

@push('style')
    <style>
        td {
            padding: 3px !important;
        }
    </style>
@endpush

@section('content')

<h5>Form  02. Formulir Evaluasi Diri Calon Mahasiswa RPL</h5>
<div class="card">
    <div class="card-body">
        <div class="text-center">
            <h4>FORMULIR EVALUASI DIRI </h4>
        </div>
        <div class="biodata mt-4">
            <div class="p-2 table-responsive">
                <table class="table table-borderless mb-0">
                    <tr>
                        <td width="20%" valign="top">NAMA PERGURUAN TINGGI</td>
                        <td width="1%" valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">PROGRAM STUDI </td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Nama Calon</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Tempat/Tgl lahir</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Alamat</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Nomor Telpon/HP</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Alamat E Mail</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Nama Mata Kuliah</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="file mt-4">
            <h4>BERKAS PENDUKUNG</h4>
            <div class="p-2 table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="30%">Nama Berkas</th>
                            <th width="30%">Berkas</th>
                            <th width="25%">Verifikasi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1.</td>
                            <td>Ijazah dan/atau Transkrip Nilai dari Mata Kuliah yang pernah ditempuh di jenjang Pendidikan Tinggi sebelumnya (khusus untuk transfer sks)</td>
                            <td>-</td>
                            <td>
                                <a href="javascript:void(0)" class="btn btn-success waves-effect btn-sm width-md waves-light mr-1 btn-verification"> Verifikasi </a>
                                <a href="javascript:void(0)" class="btn btn-danger waves-effect btn-sm width-md waves-light btn-reject"> Tolak </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="proof mt-4">
            <h4>BUKTI PENDUKUNG</h4>
            <div class="p-2 table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="5%" rowspan="2" class="align-middle">No</th>
                            <th rowspan="2" class="align-middle">Kemampuan Akhir Yang Diharapkan / <br> Capaian Pembelajaran Mata Kuliah</th>
                            <th colspan="3" class="align-middle">Profiesiensi <br> pengetahuan dan <br> keterampilan saat ini</th>
                            <th colspan="4" class="align-middle">Hasil evaluasi  <br> Asesor <br> (diisi oleh Asesor)</th>
                            <th colspan="2" class="align-middle">Bukti yang disampaikan</th>
                        </tr>
                        <tr>
                            <th>Sangat <br> baik</th>
                            <th>Baik</th>
                            <th>Tidak <br> pernah</th>
                            <th>V</th>
                            <th>A</th>
                            <th>T</th>
                            <th>M</th>
                            <th>Nomor <br> Dokumen </th>
                            <th> Jenis Dokumen (dalam <br> bentuk file PDF) </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1.</td>
                            <td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro, temporibus impedit laborum aspernatur tenetur nobis neque, dolorum aliquam obcaecati ipsam at sed quam quis officia, ut sequi odit! Mollitia, aliquid.</td>
                            <td class="text-center"> <input type="checkbox"> </td>
                            <td class="text-center"> <input type="checkbox"> </td>
                            <td class="text-center"> <input type="checkbox"> </td>
                            <td class="text-center"> <input type="checkbox"> </td>
                            <td class="text-center"> <input type="checkbox"> </td>
                            <td class="text-center"> <input type="checkbox"> </td>
                            <td class="text-center"> <input type="checkbox"> </td>
                            <td class="text-center"> DOC.1299/XI/2018 </td>
                            <td class="text-center"> <a href="#">Berkas</a> </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="d-flex justify-content-between">
            <div>
                <a href="javascript:void(0)" class="btn btn-lighten-secondary waves-effect width-md btn-back">Kembali</a>
            </div>
            <div>
                <a href="javascript:void(0)" class="btn btn-info waves-effect width-md waves-light btn-next"> Lanjut </a>
            </div>
        </div>
    </div>
</div>

@include('admin.verification.form2.modal')
@endsection

@push('script')
@include('admin.verification.form2.script')
@endpush

<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">
    <div class="slimscroll-menu">
        <!-- User box -->
        <div class="user-box text-center">
            {{-- <img src="{{asset('assets/images/users/user-1.jpg')}}" alt="user-img" title="Mat Helme" class="rounded-circle img-thumbnail avatar-lg"> --}}
            <p class="text-muted">PMB</p>
        </div>
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                <li class="menu-title">Navigation</li>
                <li>
                    <a href="{{url('admin/dashboard')}}">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span> Dashboard </span>
                    </a>
                </li>
                {{-- @can('master_candidate') --}}
                <li class="menu-title">Data Kandidat</li>
                <li>
                    <a href="javascript: void(0);">
                        <i class="fas fa-user-friends"></i>
                        <span> Data Kandidat </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        {{-- @can('candidate') --}}
                            <li><a href="{{url('admin/candidate')}}">Kandidat</a></li>
                        {{-- @endcan --}}
                    </ul>
                </li>
                {{-- @endcan --}}
            </ul>
        </div>
        <!-- End Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->
</div>
<!-- Left Sidebar End -->

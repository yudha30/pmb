<div class="modal fade" id="modal_candidate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="" autocomplete="off" id="form_candidate">
                <div class="modal-header">
                    <h4 class="modal-title">Kandidat</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Kelompok<span class="text-danger">*</span></label>
                        <select name="candidate_group_id" id="candidate-groups" required></select>
                    </div>
                    <div class="form-group">
                        <label>Posisi<span class="text-danger">*</span></label>
                        <select name="candidate_position_id" id="candidate-positions" required></select>
                    </div>
                    <div class="form-group">
                        <label>NIM<span class="text-danger">*</span></label>
                        <input type="text" name="nim" parsley-trigger="change" required
                            placeholder="NIM" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Nama<span class="text-danger">*</span></label>
                        <input type="text" name="name" parsley-trigger="change" required
                            placeholder="Nama" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Program Studi<span class="text-danger">*</span></label>
                        <select name="study_program_id" id="study-programs" required></select>
                    </div>
                    <div class="form-group">
                        <label>Tahun Masuk<span class="text-danger">*</span></label>
                        <input type="text" name="batch" parsley-trigger="change" required
                            placeholder="Tahun Masuk" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Jenis Kelamin<span class="text-danger">*</span></label>
                        <select name="gender" id="" class="form-control" required>
                            <option value="">Pilih Jenis Kelamin</option>
                            <option value="1">Laki-laki</option>
                            <option value="0">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Gambar<span class="text-danger">*</span></label>
                        <div id="image"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light btn-loading">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@extends('landing_page.layout')

@section('content')
<div class="pix_section pix-padding-v-65 pix-ebook-1">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="pix-content">
                    <img src="images/ebook/book-mockup.png" alt="" class="img-responsive">
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="pix-content pix-padding-h-10 pix-padding-top-20 pix-padding-bottom-20">
                    <h2 class="pix-white">
                        <span class="pix_edit_text">Penerimaan Mahasiswa Baru</span>
                    </h2>
                    <p class="pix-brown-2 big-text">
                        <span class="pix_edit_text">POLBANGTAN Yogyakarta - Magelang</span><br>
                        <span class="pix_edit_text">Tahun Ajaran 2021/2022</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section gray-bg pix-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="pix-content pix-padding-bottom-30 text-center">
                    <h1 class="pix-black-gray-dark text-center pix-no-margin-top pix-md-width-text">
                        <span class="pix_edit_text">Politeknik Pembangunan Pertanian Yogyakarta-Magelang</span>
                    </h1>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="pix-content pix-padding-top-20 text-justify">
                    <p class="pix-black-gray-light big-text pix-margin-bottom-20" style="text-indent: 0.5in;">
                        <span class="pix_edit_text">
                            Lembaga Pendidikan Tinggi Vokasi Lingkup Kementerian Pertanian mengemban tugas untuk menghasilkan tenaga ahli terapan bidang pertanian, peternakan, perkebunan, serta keteknikan pertanian yang mampu menjadi wirausaha muda pertanian maupun pekerja yang handal dan profesional di bidang pertanian. Penyiapan SDM pertanian yang maju, mandiri dan professional akan dapat meningkatkan produktivitas pertanian.
                        </span>
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="pix-content pix-padding-top-20 text-justify">
                    <p class="pix-black-gray-light big-text pix-margin-bottom-20" style="text-indent: 0.5in;">
                        <span class="pix_edit_text">
                            Untuk menghasilkan lulusan yang berkualitas diawali dengan penjaringan calon mahasiswa. Guna mendapatkan mahasiswa yang memenuhi persyaratan yang ditetapkan dan kelak akan berkecimpung secara professional di sektor pertanian, maka diperlukan petunjuk pelaksanaan Penerimaan Mahasiswa Baru Program Percepatan Peningkatan Kualifikasi Pendidikan Melalui Rekognisi Pembelajaran Lampau Pada Lembaga Pendidikan Tinggi Vokasi Lingkup Kementerian Pertanian.
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section" style="padding-top: 100px !important" id="step">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="pix-content text-center">
                    {{-- <div class="pix-margin-v-30">
                        <img src="images/ebook/book-mockup-inner.png" alt="" class="img-responsive pix-inline-block">
                    </div> --}}
                    <h1 class="pix-black-gray-dark text-center pix-no-margin-top pix-xs-width-text pix-xm-lineheight">
                        <span class="pix_edit_text">Alur Pendaftaran</span>
                    </h1>
                    <p class="pix-black-gray-light big-text-20 text-center pix-small2-width-text pix-no-margin-top">
                        <span class="pix_edit_text">Alur pendaftaran calon peserta Program Pendidikan RPL pada lembaga pendidikan tinggi vokasi lingkup Kementerian Pertanian:</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section pix-padding-bottom-40" style="margin-top:2%">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <img src="{{ asset('landing_page/asset/image/asset1.svg') }}" alt="" width="100%">
            </div>
            <div class="col-lg-6">
                <div width="100%">
                    <div class="icon" style="float:left; padding-top: 6px">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M17 13v1c0 2.77-.664 5.445-1.915 7.846l-.227.42-1.747-.974c1.16-2.08 1.81-4.41 1.882-6.836L15 14v-1h2zm-6-3h2v4l-.005.379a12.941 12.941 0 0 1-2.691 7.549l-.231.29-1.55-1.264a10.944 10.944 0 0 0 2.471-6.588L11 14v-4zm1-4a5 5 0 0 1 5 5h-2a3 3 0 0 0-6 0v3c0 2.235-.82 4.344-2.271 5.977l-.212.23-1.448-1.38a6.969 6.969 0 0 0 1.925-4.524L7 14v-3a5 5 0 0 1 5-5zm0-4a9 9 0 0 1 9 9v3c0 1.698-.202 3.37-.597 4.99l-.139.539-1.93-.526c.392-1.437.613-2.922.658-4.435L19 14v-3A7 7 0 0 0 7.808 5.394L6.383 3.968A8.962 8.962 0 0 1 12 2zM4.968 5.383l1.426 1.425a6.966 6.966 0 0 0-1.39 3.951L5 11 5.004 13c0 1.12-.264 2.203-.762 3.177l-.156.29-1.737-.992c.38-.665.602-1.407.646-2.183L3.004 13v-2a8.94 8.94 0 0 1 1.964-5.617z" fill="rgba(253,189,16,1)"/></svg>
                    </div>
                    <div style="margin-left:7%">
                        <h4 class="pix-black-gray-dark pix-no-margin-top pix-no-margin-bottom">
                            <span class="pix_edit_text">Membuat Akun</span>
                        </h4>
                        <p>Calon peserta wajib membuat akun aplikasi PMB pada website Polbangtan.</p>
                    </div>
                </div>
                <div width="100%">
                    <div style="float:left; padding-top: 6px">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M4 19h16v-7h2v8a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-8h2v7zM14 9h5l-7 7-7-7h5V3h4v6z" fill="rgba(253,189,16,1)"/></svg>
                    </div>
                    <div style="margin-left:7%">
                        <h4 class="pix-black-gray-dark pix-no-margin-top pix-no-margin-bottom">
                            <span class="pix_edit_text">Unduh Formulir Pendaftaran</span>
                        </h4>
                        <p>Calon peserta mengunduh formulir-formulir pendaftaran di aplikasi PMB pada website Polbangtan, seperti yang tercantum di Persyaratan Khusus.</p>
                    </div>
                </div>
                <div width="100%">
                    <div style="float:left; padding-top: 6px">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M19 22H5a3 3 0 0 1-3-3V3a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v12h4v4a3 3 0 0 1-3 3zm-1-5v2a1 1 0 0 0 2 0v-2h-2zm-2 3V4H4v15a1 1 0 0 0 1 1h11zM6 7h8v2H6V7zm0 4h8v2H6v-2zm0 4h5v2H6v-2z" fill="rgba(253,189,16,1)"/></svg>
                    </div>
                    <div style="margin-left:7%">
                        <h4 class="pix-black-gray-dark pix-no-margin-top pix-no-margin-bottom">
                            <span class="pix_edit_text">Lengkapi Formulir</span>
                        </h4>
                        <p>Calon peserta melengkapi isian formulir.</p>
                    </div>
                </div>
                <div width="100%">
                    <div style="float:left; padding-top: 6px">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M4 19h16v-7h2v8a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-8h2v7zM14 9v6h-4V9H5l7-7 7 7h-5z" fill="rgba(253,189,16,1)"/></svg>
                    </div>
                    <div style="margin-left:7%">
                        <h4 class="pix-black-gray-dark pix-no-margin-top pix-no-margin-bottom">
                            <span class="pix_edit_text">Upload Dokumen</span>
                        </h4>
                        <p>Calon peserta mendaftar di aplikasi PMB dan mengupload semua dokumen yang dipersyaratkan.</p>
                    </div>
                </div>
                <div width="100%">
                    <div style="float:left; padding-top: 6px">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M11 2c4.968 0 9 4.032 9 9s-4.032 9-9 9-9-4.032-9-9 4.032-9 9-9zm0 16c3.867 0 7-3.133 7-7 0-3.868-3.133-7-7-7-3.868 0-7 3.132-7 7 0 3.867 3.132 7 7 7zm8.485.071l2.829 2.828-1.415 1.415-2.828-2.829 1.414-1.414z" fill="rgba(253,189,16,1)"/></svg>
                    </div>
                    <div style="margin-left:7%">
                        <h4 class="pix-black-gray-dark pix-no-margin-top pix-no-margin-bottom">
                            <span class="pix_edit_text">Pantau Informasi</span>
                        </h4>
                        <p>Calon peserta wajib memantau informasi tindak lanjut PMB pada website Polbangtan.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section pix-padding-v-40 gray-bg" style="padding-top: 100px !important" id="schedule">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="pix-content pix-padding-bottom-30 text-center">
                    <h1 class="pix-black-gray-dark text-center pix-no-margin-top pix-xs-width-text pix-xm-lineheight">
                        <span class="pix_edit_text">Jadwal Pendaftaran</span>
                    </h1>
                    <p class="pix-black-gray-light big-text-20 text-center pix-small2-width-text pix-no-margin-top">
                        <span class="pix_edit_text">Berikut merupakan detail jadwal pelaksanaan PMB RPL POLBANGTAN Yogyakarta - Magelang</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        9 - 21 Agustus 2021
                    </div>
                    <div class="panel-body">
                        Pendaftaran <br>
                        &nbsp;
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        22 - 27 Agustus 2021
                    </div>
                    <div class="panel-body">
                        Penilaian Administrasi (persyaratan umum & khusus)
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        30 Agustus 2021
                    </div>
                    <div class="panel-body">
                        Pengumuman hasil seleksi administrasi <br>
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        1 – 4 September 2021
                    </div>
                    <div class="panel-body">
                        Upload bukti Portofolio untuk asesmen mandiri
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        6 – 18 September 2021
                    </div>
                    <div class="panel-body">
                        Pelaksanaan asesmen <br>
                        &nbsp;
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        19 – 20 September 2021
                    </div>
                    <div class="panel-body">
                        Banding <br>
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        24 September 2021
                    </div>
                    <div class="panel-body">
                        Pengumunan hasil PMB <br>
                        &nbsp;
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        25 – 30 September 2021
                    </div>
                    <div class="panel-body">
                        Registrasi <br>
                        &nbsp;
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Minggu pertama Oktober 2021
                    </div>
                    <div class="panel-body">
                        Perkuliahan <br>
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section pix-padding" style="padding-top: 100px !important" id="requirement">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="pix-content pix-padding-bottom-10 text-center">
                    <h1 class="pix-black-gray-dark text-center pix-no-margin-top pix-xs-width-text pix-xm-lineheight">
                        <span class="pix_edit_text">Berkas dan Persyaratan</span>
                    </h1>
                    <p class="pix-black-gray-light big-text-20 text-center pix-small2-width-text pix-no-margin-top">
                        <span class="pix_edit_text">Calon mahasiswa wajib mempersiapkan dokumen pendukung pendaftaran.</span>
                    </p>
                </div>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content pix-padding-v-30 pix-margin-v-10 pix-margin-h-40 pix-padding-h-30">
                    <div class="text-center">
                        <h4 class="pix-black-gray-dark pix-no-margin-top pix-no-margin-bottom">
                            <span class="pix_edit_text">Berkas Pendukung Pendaftaran</span>
                        </h4>
                    </div>
                    <ol class="pix-black-gray-light">
                        <li style="margin-top:10px">Formulir-formulir pendaftaran yang sudah diisi oleh calon mahasiswa.</li>
                        <li style="margin-top:10px">
                            Formulir Evaluasi Diri yang sudah diisi oleh calon mahasiswa disertai jenis bukti yang relevan dengan Capaian Pembelajaran Mata Kuliah (CPMK) antara lain :
                            <ul>
                                <li style="margin-top:10px">Ijazah dan/atau Transkrip Nilai dari Mata Kuliah yang pernah ditempuh di jenjang Pendidikan Tinggi sebelumnya (khusus untuk transfer sks).</li>
                                <li style="margin-top:10px">Daftar Riwayat pekerjaan dengan rincian tugas yang dilakukan.</li>
                                <li style="margin-top:10px">Sertifikat Kompetensi.</li>
                                <li style="margin-top:10px">Sertifikat pengoperasian/lisensi yang dimiliki.</li>
                                <li style="margin-top:10px">Foto pekerjaan yang pernah dilakukan.</li>
                                <li style="margin-top:10px">Buku harian.</li>
                                <li style="margin-top:10px">Lembar tugas / lembar kerja ketika bekerja.</li>
                                <li style="margin-top:10px">Dokumen analisis/perancangan (parsial atau lengkap) ketika bekerja di perusahaan.</li>
                                <li style="margin-top:10px">Logbook.</li>
                                <li style="margin-top:10px">Catatan pelatihan di lokasi tempat kerja.</li>
                                <li style="margin-top:10px">Keanggotaan asosiasi profesi yang relevan.</li>
                                <li style="margin-top:10px">Referensi / surat keterangan/ laporan verifikasi pihak ketiga dari pemberi kerja / supervisor.</li>
                                <li style="margin-top:10px">Penghargaan dari industri.</li>
                                <li style="margin-top:10px">Penilaian kinerja dari instansi.</li>
                                <li style="margin-top:10px">Dokumen lainnya yang mendukung.</li>
                            </ul>
                        </li>
                    </ol>
                </div>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content pix-radius-3 gray-bg pix-padding-v-30 pix-margin-v-10 pix-margin-h-40 pix-padding-h-30">
                    <div class="text-center">
                        <h4 class="pix-black-gray-dark pix-no-margin-top pix-no-margin-bottom">
                            <span class="pix_edit_text">Persyaratan Umum</span>
                        </h4>
                    </div>
                    <ol class="pix-black-gray-light">
                        <li style="margin-top:10px">Warga Negara Indonesia (WNI).</li>
                        <li style="margin-top:10px">Memiliki ijazah lulusan SLTA sederajat, atau Diploma II  bidang pertanian</li>
                        <li style="margin-top:10px">Memiliki Surat Keputusan pengangkatan menjadi ASN PPPK Penyuluh Pertanian</li>
                        <li style="margin-top:10px">Telah menjalankan pekerjaan di bidang penyuluhan paling sedikit 5 (lima) tahun pada saat dilakukan asesmen RPL</li>
                        <li style="margin-top:10px">Pada tanggal 31 Agustus 2021, berumur maksimal 52 tahun.</li>
                    </ol>
                </div>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12 column ui-droppable">
                <div class="pix-content pix-padding-v-30 pix-margin-v-10 pix-margin-h-40 pix-padding-h-30">
                    <div class="text-center">
                        <h4 class="pix-black-gray-dark pix-no-margin-top pix-no-margin-bottom">
                            <span class="pix_edit_text">Persyaratan Khusus</span>
                        </h4>
                    </div>
                    <ol class="pix-black-gray-light">
                        <li style="margin-top:10px">Melampirkan surat ijin mengikuti Tugas Belajar Program Pendidikan RPL dari pimpinan instansi (setara eselon 2).</li>
                        <li style="margin-top:10px">
                            Melampirkan surat pernyataan kesediaan :
                            <ul>
                                <li style="margin-top:10px">Menyelesaikan pendidikan Program Pendidikan RPL sampai dinyatakan lulus.</li>
                                <li style="margin-top:10px">Mengabdi di tempat tugas setelah selesai pendidikan, dan diketahui pimpinan instansi.</li>
                            </ul>
                        </li>
                        <li style="margin-top:10px">Mengisi formulir pendaftaran mahasiswa baru (form 1).</li>
                        <li style="margin-top:10px">Mengisi formulir Evaluasi Diri (form 2).</li>
                        <li style="margin-top:10px">Melengkapi curiculum vitae (form 3).</li>
                        <li style="margin-top:10px">Surat Keterangan Sehat Jasmani dan Rohani dari Dokter Rumah sakit Pemerintah/Puskesmas (terlampir pada Form 10).</li>
                        <li style="margin-top:10px">Surat Pernyataan Mentaati Peraturan Akademik pada Lembaga Pendidikan Tinggi Vokasi lingkup Kementerian Pertanian (terlampir pada Form 11).</li>
                        <li style="margin-top:10px">Membuat Surat Perjanjian Tugas Belajar Dalam Negeri Pegawai Lingkup Pertanian setelah dinyatakan diterima (terlampir pada Form 12).</li>
                        <li style="margin-top:10px">Memiliki bukti dokumen portofolio.</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section pix-padding-v-85 gray-bg" style="padding-top: 100px !important" id="download">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 text-center">
                <img src="{{ asset('landing_page/asset/image/asset2.svg') }}" alt="" height="360">
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="pix-content pix-padding-top-20 pix-margin-bottom-30">
                    <h3 class="pix-black-gray-dark pix-no-margin-top pix-sm-lineheight">
                        <span class="pix_edit_text">Petunjuk Pelaksanaan</span>
                    </h3>
                    <p class="pix-black-gray-light">
                        Petunjuk pelaksanaan Penerimaan Mahasiswa Baru Program Pendidikan RPL dapat diunduh
                       <u><a href="https://box.gmedia.id/s/Zkpbezk68FkkzqK/download?path=%2F&files=JUklak%20PMB%20RPL-edit.docx" style="color: blue">Disini</a></u>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <div class="pix_section pix-padding-v-40">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="pix-content pix-padding-h-30 pix-padding-v-30 text-center pix-margin-v-10">
                    <div class="pix-round-shape-200">
                        <img src="images/startup/client-2.jpg" alt="">
                    </div>
                    <h3 class="pix-black-gray-dark pix-no-margin-bottom">
                        <span class="pix_edit_text">Sara .H Smith</span>
                    </h3>
                    <h6 class="pix-yellow pix-no-margin-top">
                        <span class="pix_edit_text"><strong>AUTHOR OF EBOOK</strong></span>
                    </h6>
                    <p class="pix-black-gray-light big-text pix-margin-bottom-30 pix-md-width-text">
                        <span class="pix_edit_text">It is a long established fact that a reader will be distracted by the read able content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more or less normal distribution of letters, content here, making it look like read able English.</span>
                    </p>
                    <div>
                        <a href="#" class="small-social">
                            <i class="pixicon-facebook3 big-icon-50 pix-slight-white"></i>
                        </a>
                        <a href="#" class="small-social">
                            <i class="pixicon-twitter4 big-icon-50 pix-slight-white"></i>
                        </a>
                        <a href="#" class="small-social">
                            <i class="pixicon-instagram4 big-icon-50 pix-slight-white"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pix_section pix-padding-v-40 gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="pix-content pix-padding-bottom-30 text-center">
                    <h1 class="pix-black-gray-dark text-center pix-no-margin-top">
                        <span class="pix_edit_text">Questions!</span>
                    </h1>
                    <p class="pix-black-gray-light big-text-20 text-center pix-small2-width-text pix-no-margin-top">
                        <span class="pix_edit_text">From logo design to website designers and develope are ready to complete perfect your custom jobs.</span>
                    </p>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="pix-content pix-padding-bottom-30 text-center text-center">
                    <h4 class="pix-black-gray-dark text-left pix-fixed-width pix-no-margin-top pix-inline-block">
                        <span class="pix_edit_text">Why to choose eBook for your next project?</span>
                    </h4>
                    <p class="pix-black-gray-light big-text text-left pix-fixed-width pix-inline-block pix-no-margin-top">
                        <span class="pix_edit_text">It is a long established fact that a reader will be distracted by the read able content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more .</span>
                    </p>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="pix-content pix-padding-bottom-30 text-center text-center">
                    <h4 class="pix-black-gray-dark text-left pix-fixed-width pix-no-margin-top pix-inline-block">
                        <span class="pix_edit_text">How to select the right landing page template?</span>
                    </h4>
                    <p class="pix-black-gray-light big-text text-left pix-fixed-width pix-inline-block pix-no-margin-top">
                        <span class="pix_edit_text">It is a long established fact that a reader will be distracted by the read able content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more .</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection

@push('script')

<script>
    $(document).ready(function(){
        // Add scrollspy to <body>
        $('body').scrollspy({target: ".navbar", offset: 50});

        // Add smooth scrolling on all links inside the navbar
        $("#pix-navbar-collapse a").on('click', function(event) {
            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function(){

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            }  // End if
        });
    });
</script>

@endpush

@extends('layouts.app')

@section('content')

<div class="container">
    <h1>this is dashboard</h1>
</div>

@include('admin.dashboard.modal')
@endsection

@push('script')
@include('admin.dashboard.script')
@endpush

<?php

namespace App\Http\Controllers\Admin\Verification;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VerificationForm2Controller extends Controller
{
    public function index()
    {
        return view('admin.verification.form2.index');
    }
}

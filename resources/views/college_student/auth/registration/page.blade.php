@extends('college_student.auth.layout.page')

@push('content')



    <!-- Content area -->
    <div class="content d-flex justify-content-center align-items-center">

        <!-- Registration form -->
        <form action="index.html" class="flex-fill">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="card mb-0">
                        <div class="card-body">
                            <div class="text-center mb-3">
                                <i
                                    class="icon-plus3 icon-2x text-success border-success border-3 rounded-pill p-3 mb-3 mt-1"></i>
                                <h5 class="mb-0">Create account</h5>
                                <span class="d-block text-muted">All fields are required</span>
                            </div>

                            <div class="form-group form-group-feedback form-group-feedback-right">
                                <input type="text" class="form-control" placeholder="Choose username">
                                <div class="form-control-feedback">
                                    <i class="icon-user-plus text-muted"></i>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group form-group-feedback form-group-feedback-right">
                                        <input type="text" class="form-control" placeholder="First name">
                                        <div class="form-control-feedback">
                                            <i class="icon-user-check text-muted"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group form-group-feedback form-group-feedback-right">
                                        <input type="text" class="form-control" placeholder="Second name">
                                        <div class="form-control-feedback">
                                            <i class="icon-user-check text-muted"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group form-group-feedback form-group-feedback-right">
                                        <input type="password" class="form-control" placeholder="Create password">
                                        <div class="form-control-feedback">
                                            <i class="icon-user-lock text-muted"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group form-group-feedback form-group-feedback-right">
                                        <input type="password" class="form-control" placeholder="Repeat password">
                                        <div class="form-control-feedback">
                                            <i class="icon-user-lock text-muted"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group form-group-feedback form-group-feedback-right">
                                        <input type="email" class="form-control" placeholder="Your email">
                                        <div class="form-control-feedback">
                                            <i class="icon-mention text-muted"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group form-group-feedback form-group-feedback-right">
                                        <input type="email" class="form-control" placeholder="Repeat email">
                                        <div class="form-control-feedback">
                                            <i class="icon-mention text-muted"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group mb-0">
                                <label class="custom-control custom-checkbox mb-2">
                                    <input type="checkbox" name="remember" class="custom-control-input" checked>
                                    <span class="custom-control-label">Send me <a href="#">&nbsp;test account
                                            settings</a></span>
                                </label>

                                <label class="custom-control custom-checkbox mb-2">
                                    <input type="checkbox" name="remember" class="custom-control-input" checked>
                                    <span class="custom-control-label">Subscribe to monthly newsletter</span>
                                </label>

                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" name="remember" class="custom-control-input">
                                    <span class="custom-control-label">Accept <a href="#">&nbsp;terms of service</a></span>
                                </label>
                            </div>
                        </div>

                        <div class="card-footer bg-transparent text-right">
                            <button type="submit" class="btn btn-teal btn-labeled btn-labeled-right"><b><i
                                        class="icon-plus3"></i></b> Create account</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- /registration form -->

    </div>
    <!-- /content area -->
@endpush

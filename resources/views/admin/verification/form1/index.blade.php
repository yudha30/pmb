@extends('layouts.app')

@push('style')
    <style>
        td {
            padding: 3px !important;
        }
    </style>
@endpush

@section('content')

<h5>Form  01. Formulir Aplikasi Rekognisi Pembelajaran Lampau (RPL)</h5>
<div class="card">
    <div class="card-body">
        <div class="text-center">
            <h4>FORMULIR APLIKASI RPL</h4>
        </div>
        <div class="p-2 mt-4">
            <table class="table table-borderless">
                <tr>
                    <td width="20%" valign="top">Program Studi</td>
                    <td width="2%" valign="top">:</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td>Jenjang</td>
                    <td valign="top">:</td>
                    <td>-</td>
                </tr>
                <tr>
                    <td>Nama Perguruan Tinggi</td>
                    <td valign="top">:</td>
                    <td>-</td>
                </tr>
            </table>
        </div>
        <h4>DATA PRIBADI</h4>
        <div class="biodata">
            <div class="p-2 table-responsive">
                <table class="table table-borderless mb-0">
                    <tr>
                        <td width="20%" valign="top">Nama Lengkap</td>
                        <td width="1%" valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Tempat / Tanggal Lahir</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Jenis Kelamin</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Status</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Kebangsaan</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Alamat Rumah</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Kode POS</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Nomor Telepon / HP</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Email</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="education">
            <h4>DATA PENDIDIKAN</h4>
            <div class="p-2 table-responsive">
                <table class="table table-borderless">
                    <tr>
                        <td width="20%" valign="top">Pendidikan terakhir</td>
                        <td width="1%" valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Nama Perguruan <br> Tinggi / Sekolah</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Program Studi</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td valign="top">Tahun Lulus</td>
                        <td valign="top">:</td>
                        <td>-</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="mt-4 subject">
            <h4>DAFTAR MATA KULIAH</h4>
            <div class="p-2 table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <th>No</th>
                        <th>Kode <br> Mata Kuliah</th>
                        <th>Nama <br> Mata Kuliah</th>
                        <th>Mengajukan  <br> RPL</th>
                        <th>Keterangan (Isikan : <br> Transfer/Perolehan SKS)</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        <div class="d-flex justify-content-between">
            <div>
                <a href="javascript:void(0)" class="btn btn-lighten-secondary waves-effect width-md btn-back">Kembali</a>
            </div>
            <div>
                <a href="javascript:void(0)" class="btn btn-info waves-effect width-md waves-light btn-next"> Lanjut </a>
            </div>
        </div>
    </div>
</div>

@include('admin.verification.form1.modal')
@endsection

@push('script')
@include('admin.verification.form1.script')
@endpush

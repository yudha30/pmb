<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admins Controller
|--------------------------------------------------------------------------
*/
use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\CandidateController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\Verification\VerificationForm1Controller;
use App\Http\Controllers\Admin\Verification\VerificationForm2Controller;
use App\Http\Controllers\Admin\Verification\VerificationForm3Controller;

/*
|--------------------------------------------------------------------------
| Candidate Controller
|--------------------------------------------------------------------------
*/
use App\Http\Controllers\Candidate\Auth\LoginController as CandidateLoginController;
use App\Http\Controllers\CollegeStudent\LoginController as CollegeStudentLoginController;
use App\Http\Controllers\CollegeStudent\RegistrationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing_page.home');
});

Route::group(['middleware' => ['guest-handling']], function () {
    Route::get('login', [CandidateLoginController::class, 'index_login']);
    Route::post('login', [CandidateLoginController::class, 'login']);
    Route::get('admin/login', [LoginController::class, 'index_login']);
    Route::post('admin/login', [LoginController::class, 'login']);
    Route::prefix('mahasiswa')->group(function(){
        Route::prefix('login')->group(function(){
            Route::get('',[CollegeStudentLoginController::class,'index'])->name('mahasiswa.login.get');
            Route::post('',[CollegeStudentLoginController::class,'store'])->name('mahasiswa.login.post');
        });
        Route::prefix('registrasi')->group(function(){
            Route::get('',[RegistrationController::class,'index'])->name('mahasiswa.registration.get');
            Route::post('',[RegistrationController::class,'store'])->name('mahasiswa.registration.post');
        });
    });
});

Route::group(['middleware' => ['auth-handling']], function () {
    Route::post('auth/login-socket', [LoginController::class, 'login_socket']);
    Route::get('logout', [LoginController::class, 'logout']);

    Route::prefix('admin')->group(function () {
        Route::get('/dashboard', [DashboardController::class, 'index']);

        Route::prefix('candidate')->group(function () {
            Route::get('', [CandidateController::class, 'index']);
        });

        // verifikasi
        Route::prefix('verification')->group(function () {

            Route::prefix('form-1')->group(function () {

                Route::get('', [VerificationForm1Controller::class, 'index']);

            });
            Route::prefix('form-2')->group(function () {

                Route::get('', [VerificationForm2Controller::class, 'index']);

            });
            Route::prefix('form-3')->group(function () {

                Route::get('', [VerificationForm3Controller::class, 'index']);

            });
        });
    });
    Route::prefix('mahasiswa')->group(function(){
        Route::prefix('pendaftaran')->group(function(){
            Route::get('',[RegistrationController::class,'index']);
        });
    });
});



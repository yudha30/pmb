<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = \App\Models\Admin::updateOrCreate([
            'email' => 'admin@example.com',
        ],
        [
            'username' => 'admin',
            'name' => 'Admin',
            'password' => \Hash::make(123456),
            'gender' => 1,
            'phone' => "0753486",
            'super' => true
        ]);
    }
}

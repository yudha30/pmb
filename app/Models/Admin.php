<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;
use App\Traits\Uuid;

class Admin extends Authenticatable
{
    use Notifiable, HasFactory, Uuid, SoftDeletes, HasRoles;

    public $incrementing = false;

    public $keyType = 'string';

    protected $fillable = [
        'username',
        'name',
        'email',
        'password',
        'gender',
        'phone',
        'super'
    ];

    protected $dates = ['deleted_at'];
}

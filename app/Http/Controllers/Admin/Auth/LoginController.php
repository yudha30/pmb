<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\Admin;

use Carbon\Carbon;
use Log;

use Redirect;

class LoginController extends Controller
{
    public function index_login()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        try {
            $this->validate($request, [
                'username' => 'required',
                'password' => 'required'
            ]);

            $loginType = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

            $login = [
                $loginType => $request->username,
                'password' => $request->password
            ];

            Log::info($login);

            if (Auth::guard('admin')->attempt($login)) {
                Log::info('Login Berhasil');
                return redirect('/admin/dashboard');
            }else{
                Log::error('Login gagal!, periksa username dan password anda.');
                return Redirect::back()->withErrors(['message' => 'Login gagal!, periksa username dan password anda.']);
            }
        } catch (Exception $e) {
            return response([
                "status" => 400,
                "message"=> $e->getMessage(),
            ]);
        }
    }

    public function login_socket()
    {
        if(Auth::guard('admin')->check()) {
            return Auth::guard('admin')->user();
        }else if(Auth::guard('candidate')->check()){
            return Auth::guard('candidate')->user();
        }else{
            return response([
                'message' => 'Unauthorize'
            ], 401);
        }
    }

    public function logout()
    {
        if(Auth::guard('admin')->check()) {
            Auth::guard('admin')->logout();
        }else if(Auth::guard('candidate')->check()){
            Auth::guard('candidate')->logout();
        }

        return redirect('/');
    }
}

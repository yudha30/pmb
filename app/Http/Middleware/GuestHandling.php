<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class GuestHandling
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(\Auth::guard('admin')->check()){
            return redirect('/');
        }else if(\Auth::guard('candidate')->check()){
            if (strpos(url()->current(), 'admin/login') !== false) {
                \Auth::guard('candidate')->logout();
                return redirect('admin/login');
            }

            return redirect('/');
        }else{
            return $next($request);
        }
    }
}

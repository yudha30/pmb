<!DOCTYPE html>
<html lang="id">
<head>
    @include('college_student.auth.layout.header')
</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-expand-lg navbar-dark bg-indigo navbar-static">
		<div class="navbar-brand ml-2 ml-lg-0">
			<a href="index.html" class="d-inline-block">
				<img src="{{ asset('global_assets/images/logo_light.png') }}" alt="">
			</a>
		</div>
        @include('college_student.auth.layout.nav')
	</div>
	<!-- /main navbar -->


	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Inner content -->
			<div class="content-inner">
                @stack('content')

                @include('college_student.auth.layout.footer')

			</div>
			<!-- /inner content -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>

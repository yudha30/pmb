<?php

namespace App\Http\Controllers\Candidate\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use Carbon\Carbon;
use Log;

use Redirect;

class LoginController extends Controller
{
    public function index_login()
    {
        return view('candidate.auth.login');
    }

    public function login(Request $request)
    {
        try {
            $this->validate($request, [
                'username' => 'required',
                'password' => 'required'
            ]);

            $loginType = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

            $login = [
                $loginType => $request->username,
                'password' => $request->password
            ];

            Log::info($login);

            if(Auth::guard('candidate')->attempt($login)){
                if(Auth::guard('candidate')->user()->active == true && Auth::guard('candidate')->user()->verified == true){

                    Log::info('Login Berhasil');

                    return redirect('/candidate/debate');
                }else{
                    Auth::guard('candidate')->logout();

                    Log::error('Login gagal!, Akun tidak aktif.');

                    return Redirect::back()->withErrors(['message' => 'Login gagal!, Akun tidak aktif.']);
                }
            }else{
                Log::error('Login gagal!, periksa username dan password anda.');
                return Redirect::back()->withErrors(['message' => 'Login gagal!, periksa username dan password anda.']);
            }
        } catch (Exception $e) {
            return response([
                "status" => 400,
                "message"=> $e->getMessage(),
            ]);
        }
    }
}
